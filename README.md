# 2024_vetter_gradient_variability_ma

R. Vetter & D. Iber, *Reply to: Assessing the precision of morphogen gradients in neural tube development*

Matters Arising Reply submitted to Nature Communications

arXiv:2402.00781, https://doi.org/10.48550/arXiv.2402.00781

relating to:

R. Vetter & D. Iber, *Precision of morphogen gradients in neural tube development*

Nature Communications 13, 1145 (2022), https://doi.org/10.1038/s41467-022-28834-3


## Contents

* Cohen2015_SHH_gradient.csv - Table with SHH gradient parameters as reported in Cohen et al., Nat. Commun. 2015
* correlation_from_fitting.m - MATLAB script that produces Fig. 1C,D
* correlation_from_fitting.csv - Data as shown in Fig. 1C,D, produced by correlation_from_fitting.m
* gradient_parameters.m - MATLAB script that produces Fig. 1A
* gradient_parameters.csv - Data as shown in Fig. 1A, produced by gradient_parameters.m
* scaling_error.m - MATLAB script that produces Fig. 2
* scaling_error.csv - Data as shown in Fig. 2, produced by scaling_error.m
* SHH_correlation.m - MATLAB script that calculates the correlation coefficients from Cohen2015_SHH_gradient.csv
* LICENSE - License file


## Installation, System requirements

The MATLAB scripts were produced with version R2023b and require the Statistics and Machine Learning Toolbox (stats). No further installations are required, and no further system limitations apply. 


## Usage instructions

The MATLAB scripts can be executed out-of-the-box by just running them in MATLAB. 


## Input/Output

No manual specification of input or output is required. Input data files, where used, are provided in this repository.
All MATLAB scripts produce as output the figures and their data files as shown in the Reply, as indicated in the list of contents above.


## Runtime

All scripts run in a matter of seconds to minutes on a normal laptop computer. Progress windows are displayed for the longer ones.


## License

All source code is released under the 3-Clause BSD license (see LICENSE for details).
