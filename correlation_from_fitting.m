% Copyright © 2024
% ETH Zurich
% Department of Biosystems Science and Engineering
% Roman Vetter & Dagmar Iber

clear all
close all

FontSize = 18;
format = '%.2f';
formatp = '%.2f';

rng("default") % for reproducibility

Nruns = 1e3;
lambda = 19.26; % SSH gradient length in µm (Fig. 2b in Cohen et al., Nat. Commun. 2015)
Cmax = 1;
Cbg = Cmax/2^8; % background
CV_C = 0.11; % noise level in the gradients (inferred with gradient_parameters.m)

SS = 5:5:30; % somite stages for binning
Lstep = 88.1 * exp(2*SS/41.6); % NT lengths (Fig. S1 in Zagorski et al., Science 2017)

T = table();
T.NT_size = (50:5:450)';
T.C0 = Cmax*exp((T.NT_size-400)/118.2764); % gradient amplitude (Fig. 7D in Vetter & Iber, Nat. Commun. 2022)

fitted_C0 = NaN(numel(T.NT_size),Nruns);
fitted_lambda = NaN(numel(T.NT_size),Nruns);
R = NaN(numel(T.NT_size),Nruns);
tau = NaN(numel(T.NT_size),Nruns);

delete(findall(0, 'type', 'figure', 'tag', 'TMWWaitbar'))
wb = waitbar(0, '0 %', 'Name', 'Progress');
tic

for i = 1:numel(T.NT_size)
    x = 0:1:round(T.NT_size(i));
    C = T.C0(i)*exp(-x/lambda);
    
    parfor j = 1:Nruns
        noise = CV_C * randn(size(x));
        Cnoisy = C.*(1+noise) - Cbg;
        mdl = fitnlm(x, Cnoisy, @(p,x) p(1)*exp(-x/p(2)), [C(1) lambda]);
        fitted_C0(i,j) = mdl.Coefficients.Estimate(1);
        fitted_lambda(i,j) = mdl.Coefficients.Estimate(2);
    end

    progress = i / numel(T.NT_size);
    waitbar(progress, wb, sprintf('%i %% (time left: %s)', floor(100 * progress), duration(0,0,toc*(1-progress)/progress)));
end

for j = 1:Nruns
    for i = 1:numel(T.NT_size)
        idx = max(i-7,1):min(i+7,numel(T.NT_size));
        R(i,j) = corr(fitted_lambda(idx,j), fitted_C0(idx,j), 'type', 'Pearson');
        tau(i,j) = corr(fitted_lambda(idx,j), fitted_C0(idx,j), 'type', 'Kendall');
    end
end

toc
close(wb)

T.R_mean = mean(R,2);
T.R_SD = std(R,0,2);
T.tau_mean = mean(tau,2);
T.tau_SD = std(tau,0,2);
T.fitted_C0_mean = mean(fitted_C0,2);
T.fitted_C0_SD = std(fitted_C0,0,2);
T.fitted_lambda_mean = mean(fitted_lambda,2);
T.fitted_lambda_SD = std(fitted_lambda,0,2);
writetable(T, 'correlation_from_fitting.csv')

figure
hold on
box on
orange = [0.929 0.694 0.125];
fill([T.NT_size; flipud(T.NT_size)], [T.R_mean + T.R_SD; flipud(T.R_mean - T.R_SD)], orange, 'FaceAlpha', 0.2, 'LineStyle', 'none', 'HandleVisibility', 'off');
plot(T.NT_size, T.R_mean, '-', 'Color', orange, 'LineWidth', 2, 'DisplayName', 'Pearson''s R')
xlabel('NT length [µm]')
ylabel({'Artificial correlation', 'between fitted \lambda and C_0'})
legend show
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
xlim([100 400])
ylim([-1 1])

c = [223 82 91; 73 86 168; 93 178 80; 105 39 133; 0 0 0]/255;
SHH = readtable('Cohen2015_SHH_gradient.csv');
R = NaN(numel(Lstep)-1,1);
for i = 1:numel(R)
    idx = find(SHH.NT_size >= Lstep(i) & SHH.NT_size <= Lstep(i+1));
    R(i) = corr(SHH.lambda(idx), SHH.C0(idx), 'type', 'Pearson');
    line([Lstep(i) Lstep(i+1)], R(i)*[1 1], 'Color', c(i,:), 'LineWidth', 2, 'HandleVisibility', 'off')
    text((Lstep(i)+Lstep(i+1))/2, R(i), sprintf(format, R(i)), 'FontSize', FontSize, 'Color', c(i,:), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom');
end

figure
hold on
box on
fill([T.C0; flipud(T.C0)], [T.fitted_lambda_mean + T.fitted_lambda_SD; flipud(T.fitted_lambda_mean - T.fitted_lambda_SD)], orange, 'FaceAlpha', 0.2, 'LineStyle', 'none', 'HandleVisibility', 'off');
plot(T.C0, T.fitted_lambda_mean, '-', 'Color', orange, 'LineWidth', 2, 'DisplayName', 'Pearson''s R')
xlabel('C_0 / C_{max}')
ylabel('Fitted \lambda [µm]')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
