% Copyright © 2024
% ETH Zurich
% Department of Biosystems Science and Engineering
% Roman Vetter & Dagmar Iber

function gradient_parameters

rng("default") % for reproducibility

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
Ngradients = 1e3; % number of independent simulation runs
diameter = 4.9; % cell diameter [µm] (DOI:10.1038/ncomms7709)
mu_D = 0.033; % mean morphogen diffusion constant [µm^2/s] (DOI:10.1371/journal.pcbi.1005052)
mu_lambda = 19.26; % mean gradient length [µm] (DOI:10.1038/ncomms7709)
mu_d = mu_D/mu_lambda^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]
ncS = 5; % number of cells in the source domain
ncP = 50; % number of cells in the patterning domain
CV = 0.3; % coefficient of variation of the kinetic parameters (CV_p,d,D)

nc = ncS + ncP; % total number of cells
LS = ncS * diameter; % source length
LP = ncP * diameter; % pattern length
C = @(x,C0,lambda) C0 * cosh((LP-x)/lambda) / cosh(LP/lambda);
C0 = mu_p/(2*mu_d) * (1-exp(-2*LS/mu_lambda)); % deterministic amplitude (initial guess for fitting)

% solver initialization
x0 = (-ncS:ncP) * diameter;
x0 = sort([x0 x0(2:end-1)]); % duplicate interface nodes
sol0 = bvpinit(x0, @y0);
options = bvpset('Vectorized', 'on', 'NMax', 100*nc, 'RelTol', tol, 'AbsTol', tol);
                    
fitopt = statset('TolFun', tol, 'TolX', tol);

% loop over independent runs
CV_C = NaN(Ngradients, 1);
fitted_lambda = NaN(Ngradients, 1);
fitted_C0 = NaN(Ngradients, 1);
for i = 1:Ngradients
    % draw random kinetic parameters for each cell
    p = random(logndist(mu_p, CV), nc, 1);
    d = random(logndist(mu_d, CV), nc, 1);
    D = random(logndist(mu_D, CV), nc, 1);

    % solve the reaction-diffusion equation
    sol = bvp4c(@odefun, @bcfun, sol0, options);

    % fit a hyperbolic cosine in log space in the patterning domain
    logcosh = @(p,x) p(2) + log(cosh((LP-x)/p(1)));
    idx = find(sol.x >= 0);
    mdl = fitnlm(sol.x(idx), log(sol.y(1,idx)), logcosh, [mu_lambda log(C0/cosh(LP/mu_lambda))], 'Options', fitopt);
    fitted_lambda(i) = mdl.Coefficients.Estimate(1);
    fitted_C0(i) = exp(mdl.Coefficients.Estimate(2)) * cosh(LP/fitted_lambda(i));

    % determine the variability in the concentrations
    xi = 0:1:floor(x0(end)); % spatial resolution of 1 µm
    [~, idx] = unique(sol.x);
    Ci = interp1(sol.x(idx), sol.y(1,idx), xi);
    Cfit = C(xi, fitted_C0(i), fitted_lambda(i));
    CV_C(i) = std((Ci - Cfit) ./ Cfit);
end

% correlation between gradient decay length and amplitude
[R, pR] = corr(fitted_lambda, fitted_C0, 'type', 'Pearson')
[tau, ptau] = corr(fitted_lambda, fitted_C0, 'type', 'Kendall')

% gradient variability
CV_C = mean(CV_C)

figure
box on
hold on
scatter(fitted_lambda, fitted_C0, 'filled')
xlabel('Decay length \lambda [µm]')
ylabel('Amplitude C_0 [arb. units]')
set(gca, 'LineWidth', 1, 'FontSize', 18)

% write results to csv file
T = table();
T.fitted_lambda = fitted_lambda;
T.fitted_C0 = fitted_C0;
writetable(T, 'gradient_parameters.csv')


%% functions for the ODE
% reaction-diffusion equation
function dydx = odefun(x, y, c)
dC = -y(2,:) / D(c); % mass flux: j = -D*grad(C)
dj = p(c) * (c <= ncS) - d(c) * y(1,:); % conservation of mass: div(j) = p*H(-x) - d*C
dydx = [dC; dj];
end

% initial guess
function y = y0(x, c)
y = [0; 0];
end

% boundary & cell interface conditions
function res = bcfun(ya, yb)
res = ya(:);
res(1) = ya(2, 1); % zero flux at the left end of the source domain
res(2) = yb(2,nc); % zero flux at right end of the patterning domain
for c = 1:nc-1
    res(2*c+1) = ya(1,c+1) - yb(1,c); % concentration continuity
    res(2*c+2) = ya(2,c+1) - yb(2,c); % flux continuity
end
end

% log-normal distribution with adjusted mean & stddev
function pd = logndist(mu, CV)
    pd = makedist('Lognormal', 'mu', log(mu/sqrt(1+CV^2)), 'sigma', sqrt(log(1+CV^2)));
end

end