% Copyright © 2024
% ETH Zurich
% Department of Biosystems Science and Engineering
% Roman Vetter & Dagmar Iber

close all
clear all

FontSize = 18;
format = '%.3f';
formatp = '%.2f';

SHH = readtable('Cohen2015_SHH_gradient.csv');
SHH = sortrows(SHH, 'NT_size');

[R, pR] = corr(SHH.lambda, SHH.C0, 'type', 'Pearson');
[tau, ptau] = corr(SHH.lambda, SHH.C0, 'type', 'Kendall');

scatter(SHH.lambda, SHH.C0, [], SHH.NT_size, 'filled')
box on
xlabel('Decay length \lambda [µm]')
ylabel('Amplitude C_0 [arb. units]')
set(gca, 'FontSize', FontSize, 'LineWidth', 1)
cb = colorbar();
ylabel(cb, 'NT length [µm]', 'FontSize', FontSize)
clim([100 400])
text(57, 90, {['Pearson''s R = ' sprintf(format, R) ' (p = ' sprintf(formatp, pR) ')'], ['Kendall''s \tau = ' sprintf(format, tau) ' (p = ' sprintf(formatp, ptau) ')']}, 'FontSize', FontSize, 'HorizontalAlignment', 'right')
