% Copyright © 2024
% ETH Zurich
% Department of Biosystems Science and Engineering
% Roman Vetter & Dagmar Iber

clear all
close all
delete(findall(0, 'type', 'figure', 'tag', 'TMWWaitbar'))

% seed the random number generator for reproducibility
rng("default")

% fixed parameters
Ngradients = 1e4; % number of gradients
diam = 4.9; % cell diameter in µm
mu_lambda = 20; % mean gradient decay length in µm
mu_C0 = 1; % mean gradient amplitude in arbitrary units


%% reproduction of the setup used by Zagorski et al.
CV_lambda = 0.2; % noise in gradient decay length
CV_C0 = 0.2; % noise in gradient amplitude
Lmin = 100; % minimum domain length in µm
Lmax = 150; % maximum domain length in µm
Ctheta = 0.1; % readout threshold

% uniformly distributed random domain lengths
L = Lmin + (Lmax - Lmin) * rand(Ngradients,1);

% normally distributed random decay lengths and amplitudes of synthetic exponential gradients
lambda = mu_lambda * (1 + CV_lambda * randn(Ngradients,1));
C0 = mu_C0 * (1 + CV_C0 * randn(Ngradients,1));

% unscaled readout positions, their mean and standard deviation (= positional error)
x = lambda .* log(C0./Ctheta); % inverse of exponentials
mu_x = mean(x)
sigma_x = std(x)

% scaled readout positions, their mean and standard deviation
x_scaled = mean(L)./L .* x;
mu_x_scaled = mean(x_scaled)
sigma_x_scaled = std(x_scaled)

% the scaling error in cell diameters, and its standard error
scaling_error_cd = (sigma_x_scaled - sigma_x) / diam
scaling_error_cd_SE = std(bootstrp(1e4, @(x1,x2) (std(x1) - std(x2)) / diam, x_scaled, x))


%% analysis along the entire patterning axis
T = table();
T.xi = (0.025:0.05:1)';

Lmid = (Lmin + Lmax) / 2;
Ctheta = mu_C0 * exp(-T.xi*Lmid/mu_lambda); % readout thresholds
scaling_error_cd = NaN(size(Ctheta));
scaling_error_cd_SE = scaling_error_cd;

wb = waitbar(0, '0 %', 'Name', 'Progress');
tic

for j = 1:numel(Ctheta)
    % uniformly distributed random domain lengths
    L = Lmin + (Lmax - Lmin) * rand(Ngradients,1);

    % gradient variability inferred from molecular noise (Fig. 8F,I in Vetter & Iber, Nat. Commun. 2022)
    CV_lambda = sqrt(6.13/Lmid * ((1+0.3^2)^(1/4) * (1+0.3^2)^(1/4) - 1));
    CV_C0 = 0.1773 + Lmid/12871 - (Lmid/5659)^2;
    
    % normally distributed random decay lengths and amplitudes of synthetic exponential gradients
    lambda = mu_lambda * (1 + CV_lambda * randn(Ngradients,1));
    C0 = mu_C0 * (1 + CV_C0 * randn(Ngradients,1));
    
    % unscaled readout positions, their mean and standard deviation (= positional error)
    x = lambda .* log(C0./Ctheta(j)); % inverse of exponentials
    sigma_x = std(x);
    
    % scaled readout positions and their standard deviation
    x_scaled = mean(L)./L .* x;
    sigma_x_scaled = std(x_scaled);
    
    % the scaling error in cell diameters, and its standard error
    difference_cd = @(x_scaled, x) (std(x_scaled) - std(x)) / diam;
    scaling_error_cd(j) = difference_cd(x_scaled, x);
    scaling_error_cd_SE(j) = std(bootstrp(1e4, difference_cd, x_scaled, x));
    
    progress = j / numel(Ctheta);
    waitbar(progress, wb, sprintf('%i %% (time left: %s)', floor(100 * progress), duration(0,0,toc*(1-progress)/progress)));
end

toc
close(wb)

% degree of additivity
alpha = fminsearch(@(alpha) norm(scaling_error_cd - alpha * (Lmax - Lmin) / (2*sqrt(3)) / diam * T.xi), 1)

figure
hold on
box on
plot([0 1], [0 3], 'k-', 'LineWidth', 2)
errorbar(T.xi, scaling_error_cd, scaling_error_cd_SE, 'o-', 'LineWidth', 2)
xlabel('Relative position \xi')
ylabel('Domain scaling error (cell diameters)')
set(gca, 'FontSize', 18, 'LineWidth', 1)
xlim([0 1])
ylim([0 3])

T.scaling_error_cd = scaling_error_cd;
T.scaling_error_cd_SE = scaling_error_cd_SE;

writetable(T, 'scaling_error.csv')
